//
//  ViewController.swift
//  mundoanimal_22101144
//
//  Created by COTEMIG on 17/11/22.
//

import UIKit
import Alamofire
import Kingfisher

struct Animal: Decodable {
    let name : String
    let latin_name : String
    let image_link : String
}

class ViewController: UIViewController {
    
    @IBOutlet var image: UIImageView!
    @IBOutlet var LatinName: UILabel!
    
    var data: Animal?
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        requestData()
    }

    private func requestData(){
        AF.request("https://zoo-animal-api.herokuapp.com/animals/rand").responseDecodable(of: Animal.self){
            response in
            if let data_request = response.value{
                self.data = data_request;
            }
            self.LatinName.text = self.data?.latin_name
            self.image.kf.setImage(with: URL(string: self.data!.image_link))
            self.title = self.data?.name
        }
    
    }   
}


   


    
       
    


